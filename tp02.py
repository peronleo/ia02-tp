from typing import List, Dict, Tuple

import itertools as iter

import sys

import os

org_stdout = sys.stdout

graph = [
    (1, 2),
    (2, 3),
    (3, 4),
    (4, 5),
    (5, 1),
    (1, 6),
    (2, 7),
    (3, 8),
    (4, 9),
    (5, 10),
    (6, 8),
    (6, 9),
    (7, 9),
    (7, 10),
    (8, 10),
]

nbrSommets = 10


def generateVocDico(n: int) -> Tuple[List[List[str]], Dict[str, int]]:
    voc = []
    dicoVoc = {}
    elem = ["R", "G", "B"]
    compteur = 1
    for i in range(1, n + 1):
        raw = []
        for x in elem:
            o = f"S{i}{x}"
            raw.append(o)
            dicoVoc[o] = compteur
            compteur += 1
        voc.append(raw)
        for l in iter.combinations(raw, 2):
            voc.append(l)
    return voc, dicoVoc


# Clauses: (Non R ou Non G) (Non R ou Non B) (Non G ou Non B) (G ou B ou R)


def generateColorClauses(voc: List[List[str]], dico: Dict[str, int]) -> None:
    for x in voc:
        o = ""
        for y in x:
            if len(x) == 3:
                o += f"{dico[y]} "
            else:
                o += f"-{dico[y]} "
        o += "0"
        print(o)


# Clauses: Non SxR ou Non SyR


def generateArcsClauses(arcs: List[Tuple[int, int]], dico: Dict[str, int]) -> None:
    for tuples in arcs:
        keys1 = [x for x in dico.keys() if str(tuples[0]) in x]
        keys2 = [x for x in dico.keys() if str(tuples[1]) in x]
        for x, y in zip(keys1, keys2):
            print(f"-{dico[x]} -{dico[y]} 0")


def get_key(val: int, dico: Dict[str, int]) -> str:
    for key, value in dico.items():
        if val == value:
            return key


def printArc(str: str, dico: Dict[str, int]) -> None:
    list = str.split()
    for word in list:
        if word != "v" and word != "0":
            if int(word) > 0:
                print(get_key(int(word), dico))


if __name__ == "__main__":
    with open("clausesfile.cnf", "w") as f:
        parameters = generateVocDico(nbrSommets)
        sys.stdout = f
        print(
            f"p cnf ${len(parameters[1].keys())} {len(parameters[1].keys())*30 + len(graph)*3}"
        )
        generateColorClauses(parameters[0], parameters[1])
        generateArcsClauses(graph, parameters[1])
        sys.stdout = org_stdout
        result = input("Résultat donné par le solver:")
        printArc(result, parameters[1])
