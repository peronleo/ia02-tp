from typing import List, Dict, Generator


def decomp(n: int, nbr_bytes: int) -> List[bool]:
    result = []
    for i in range(nbr_bytes):
        result.append(n % 2 == 1)
        n //= 2
    return result


def interpretation(voc: List[str], vals: List[bool]) -> Dict[str, bool]:
    dico = {}
    for i in range(len(voc)):
        dico[voc[i]] = vals[i]
    return dico


def genInterpretations(voc: List[str]) -> Generator[Dict[str, bool], None, None]:
    for i in range(2 ** len(voc)):
        vals = decomp(i, len(voc))
        yield interpretation(voc, vals)


def valuate(formula: str, interpretation: Dict[str, bool]) -> bool:
    return eval(formula, interpretation)


def printTable(formula: str, vocab: List[str]) -> None:

    string = ""
    for i in vocab:
        string += "|    " + i + "    "
    string += "|"
    print(f"{string} {formula}")

    for i in genInterpretations(vocab):
        string = ""
        for x in vocab:
            string += "|  " + str(i[x]) + "  "
        string += "|"
        print(f"{string} -> {valuate(formula, i)}")


def isValide(formula: str, vocab: List[str]) -> bool:
    for i in genInterpretations(vocab):
        if not valuate(formula, i):
            return False
    return True


def isContradictoire(formula: str, vocab: List[str]) -> bool:
    for i in genInterpretations(vocab):
        if valuate(formula, i):
            return False
    return True


def isContingeante(formula: str, vocab: List[str]) -> bool:
    if isContradictoire(formula, vocab):
        return False
    for i in genInterpretations(vocab):
        if not valuate(formula, i):
            return True
    return False


def isCons(f1: str, f2: str, voc: List[str]) -> bool:
    pass


if __name__ == "__main__":
    printTable("(A or B) and not C or D", ["A", "B", "C", "D"])
    print(isValide("(A or B) and not C or D", ["A", "B", "C", "D"]))
    print(isContradictoire("(A or B) and not C or D", ["A", "B", "C", "D"]))
    print(isContingeante("(A or B) and not C or D", ["A", "B", "C", "D"]))
