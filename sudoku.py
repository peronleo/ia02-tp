import requests
import json
import unidecode as uc
from bs4 import BeautifulSoup
from random import randrange
from typing import List, Tuple, Dict
import tp03 as sk
from datetime import datetime as dt


def main():
    joueur = ""
    sudoku = None
    dataset = loadDatas()
    while joueur != "0" or joueur not in switcher.keys():
        print("----------------------------")
        print("   Jeu de Sudoku CONSOLE")
        print("----------------------------")
        print("     Choix du joueur:")
        print("1.Jouer à un Sudoku")
        print("2.Statistique du joueur")
        print("3.Crédits")
        print("0.Quitter le jeu")
        print("----------------------------")
        joueur = input("\nVeuillez entrer votre choix:\n")
        if joueur == "1":
            id = input("Quel est votre nom ?\n")
            jouerSudoku(sudoku, id, dataset)
        elif joueur == "2":
            id = input("Quel est votre nom ?")
            printStat(id, dataset)
        elif joueur == "3":
            printCredits()
        elif joueur == "0":
            pushToJson(dataset)
            return


# Fonctions UTILS


def polyMatrice(mat: List[List[int]]) -> List[List[int]]:
    goodmat: List[List[int]] = [[] for i in range(9)]
    x, y, line = 0, 0, 0
    for submat in mat:
        x += 1
        dx, dy = 0, 0
        for var in submat:
            if dx == 3:
                dx = 0
                line += 1
            goodmat[line].append(int(var))
            dx += 1
        if x == 3:
            x = 0
            y += 1
        line = 3 * y
    return goodmat


def printCredits() -> None:
    print(
        "----------------------------------------------------------------------------"
    )
    print("           Interface de jeu d'un Sudoku dans la console - Léo Péron")
    print(
        "----------------------------------------------------------------------------"
    )
    print("-Résolution par SAT vu dans le cadre de l'UV IA02")
    print("       Responsable: Sylvain Lagrue")
    print("       SAT Solver: Gophersat")
    print(
        "-Génération d'un Sudoku en parsant un générateur en ligne et en reformattant"
    )
    print("       Site: http://tadkozh.free.fr")
    print("       Auteur: Tadkozh")
    print("-Système de Statistique avec mémoire par JSON")
    print(
        "----------------------------------------------------------------------------"
    )


# Fonctions JSON


def pushToJson(dataset: Dict[str, Dict[str, int]]) -> None:
    with open("data_sudoku.json", "w") as json_file:
        json_file.write(json.dumps(dataset))


def loadDatas():
    try:
        with open("data_sudoku.json", "r") as json_file:
            return json.loads(json_file.read())
    except OSError:
        with open("data_sudoku.json", "w") as json_file:
            json_file.write("{}")
        with open("data_sudoku.json", "r") as json_file:
            return json.loads(json_file.read())


def checkSudoku(sudoku: List[List[int]], correct: List[List[int]]) -> bool:
    for sub1, sub2 in zip(sudoku, correct):
        for x1, x2 in zip(sub1, sub2):
            if x1 != x2:
                return False
    return True


# Fonctions Stats


def insertStat(
    id: str, state: str, dataset: Dict[str, Dict[str, int]]
) -> Dict[str, Dict[str, int]]:
    template = {
        "nbrParties": 0,
        "sudokuRes": 0,
        "facile": 0,
        "moyen": 0,
        "difficile": 0,
    }
    id = uc.unidecode(id.lower())
    if not id in dataset.keys():
        dataset[id] = template

    dataset[id][state] += 1

    return dataset


def printStat(id: str, dataset: Dict[str, Dict[str, int]]) -> None:
    fetch = uc.unidecode(id.lower())
    if not fetch in dataset.keys():
        print("----------------------")
        return print("Joueur non existant :(")
    raw = dataset[fetch]
    print("---------------------------")
    print(f"Statistique du Joueur: {id}")
    print(f'Nombre de parties: {raw["nbrParties"]}')
    print(f'Sudoku résolus: {raw["sudokuRes"]}')
    print(f'Moyenne résolution: {raw["sudokuRes"]/raw["nbrParties"]*100}%')
    print(f'Sudoku Facile: {raw["facile"]}')
    print(f'Sudoku Moyen: {raw["moyen"]}')
    print(f'Sudoku Difficile: {raw["difficile"]}')


# Génération Sudoku (Parsing)


def genSudoku(id: str, dataset: Dict[str, Dict[str, int]]) -> List[List[int]]:
    level = {"Facile": 1, "Moyen": 2, "Difficile": 3}
    choix = ""
    while choix not in level.keys():
        choix = input(f"Quelle dificulté souhaitez vous ? {list(level.keys())}\n")

    insertStat(id, choix.lower(), dataset)
    # Start parsing
    req = requests.get(
        f"http://tadkozh.free.fr/sudokus/generateursudokus.php?grille={randrange(1262)}&niveau={level[choix]}"
    )
    soup = BeautifulSoup(req.text, "lxml")

    # Getting and stocking cells values
    mat = []
    squares = soup.find_all("table", class_="carre")
    for subsquare in squares:
        cells = subsquare.find_all("td", class_="cellule")
        submat = [e.string.replace("\xa0", "0") for e in cells]
        mat.append(submat)

    return polyMatrice(mat)


# Fonctions de jeu du Sudoku


def jouerSudoku(
    sudoku: List[List[int]], id: str, dataset: Dict[str, Dict[str, int]]
) -> None:

    joueur = ""
    tour = 0
    startTime = dt.now()
    if sudoku == None:
        sudoku = genSudoku(id, dataset)
    raw = sk.solve_sudoku(sudoku)

    insertStat(id, "nbrParties", dataset)

    # Check s'il n'y a pas d'erreur
    if not raw[0]:
        print("Erreur")
        return

    correctSudoku = raw[1]

    while not checkSudoku(sudoku, correctSudoku) and joueur != "0":
        temps = dt.now() - startTime
        print("-------------------------")
        print("  Jeu de Sudoku CONSOLE")
        print(f"Tour: {tour}")
        print(f"Temps: {temps}")
        sk.print_sudoku(sudoku)
        print("    Choix du joueur:")
        print("1.Insérer")
        print("2.Indice")
        print("3.Réponse")
        print("0.Abandonner")
        joueur = input(f"\nVeuillez entrer votre choix {id}:\n")
        if joueur == "1":
            sudoku = actionSudoku(sudoku, correctSudoku)
        elif joueur == "2":
            insertIndice(sudoku, correctSudoku)
        elif joueur == "3":
            print("La réponse était:")
            sk.print_sudoku(correctSudoku)
            return
        elif joueur == "0":
            return
        if joueur in ["1", "2", "3"]:
            tour += 1

    if checkSudoku(sudoku, correctSudoku):
        print("Vous avez gagné le Sudoku !")
        insertStat(id, "sudokuRes", dataset)
        return


"""
def actionSudoku(sudoku: List[List[int]], correct: List[List[int]]) -> List[List[int]]:

    x = int(input("En quelle coords voulez-vous répondre ? (X)\n"))
    y = int(input("En quelle coords voulez-vous répondre ? (Y)\n"))

    while (x not in [0,8] or y not in [0,8]) and sudoku[x][y] != 0:
        x = int(input("En quelle coords voulez-vous répondre ? (X)\n"))
        y = int(input("En quelle coords voulez-vous répondre ? (Y)\n"))

    essai = int(input("Quel nombre voulez-vous insérer ?\n"))

    while essai < 0 or essai > 9:
        essai = int(input("Quel nombre voulez-vous insérer ?\n"))

    if essai == correct[x][y]:
        sudoku[x][y] = essai
        print("Bonne réponse !")
    else:
        print("Mauvaise réponse !")

    return sudoku
"""


def actionSudoku(sudoku: List[List[int]], correct: List[List[int]]) -> List[List[int]]:

    answers = {}
    count = 1
    choice = -1
    essai = -1
    for x in range(len(sudoku)):
        for y in range(len(sudoku)):
            if sudoku[x][y] == 0:
                answers[count] = (x, y)
                count += 1

    print(
        "---------------------------------------------------------------------------------------------------"
    )
    o = ""
    old = 0
    for key in answers.keys():
        if not key // 10:
            o += " "
        if not key % 10:
            o += "\n"
        o += f"{key}.{answers[key]} "
    print(o)
    print(
        "---------------------------------------------------------------------------------------------------"
    )

    while choice not in answers.keys():
        choice = int(input("Veuillez choisir les coordonnées d'insertion\n"))

    coords = answers[choice]

    while essai < 0 or essai > 9:
        essai = int(input("Quel nombre voulez-vous insérer ?\n"))

    if essai == correct[coords[0]][coords[1]]:
        sudoku[coords[0]][coords[1]] = essai
        print("Bonne réponse !")
    else:
        print("Mauvaise réponse !")

    return sudoku


def insertIndice(sudoku: List[List[int]], correct: List[List[int]]) -> List[List[int]]:
    x, y = randrange(9), randrange(9)
    while sudoku[x][y] != 0:
        x, y = randrange(9), randrange(9)
    sudoku[x][y] = correct[x][y]
    print(f"Indice inséré en [{x},{y}]")
    return sudoku


if __name__ == "__main__":
    main()
