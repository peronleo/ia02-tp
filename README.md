# IA02 - TP

Fichiers des TPs d'IA02

## TP01 - Model Checking

Réalisation d'un programme qui affiche la table de vérité d'une formule donné en argument et un ensemble de variables propositionnelles. Le programme se limite aux opérateurs AND, OR et NOT.

## TP02 - DIMACS

Utilisation de SATs afin de résoudre des problèmes logiques. Le programme résout un problème de coloration de graphe avec en entrée le nombre de sommets et la liste des arcs.

## TP03 - Résolution de Sudoku en SAT

Résoudre n’importe quel Sudoku à l’aide d’un solver SAT, via Python. 

## Sudoku.py - Interface jeu du TP03

Réalisation d'une interface de jeu afin de pouvoir jouer au Sudoku (la réponse étant générée par le TP03)
- Utilisation de Beautifulsoup pour récupérer des Sudokus déjàs initialisés avec différentes difficultés
- Utilisation de JSON pour mettre en place un système de statistiques 
