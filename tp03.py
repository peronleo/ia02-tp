from typing import List, Tuple
import subprocess
from pprint import pprint
import itertools as iter

# Alias de types
Variable = int
Literal = int
Clause = List[Literal]
Model = List[Literal]
Clause_Base = List[Clause]
Grid = List[List[int]]

example: Grid = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
]


example2: Grid = [
    [0, 0, 0, 0, 2, 7, 5, 8, 0],
    [1, 0, 0, 0, 0, 0, 0, 4, 6],
    [0, 0, 0, 0, 0, 9, 0, 0, 0],
    [0, 0, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 5, 0, 2, 0],
    [0, 0, 0, 8, 1, 0, 0, 0, 0],
    [4, 0, 6, 3, 0, 1, 0, 0, 9],
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [7, 2, 0, 0, 0, 0, 3, 1, 0],
]


empty_grid: Grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
]

# Fonctions fournies


def write_dimacs_file(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)


def exec_gophersat(
    filename: str, cmd: str = "gophersat", encoding: str = "utf8"
) -> Tuple[bool, List[int]]:
    result = subprocess.run(
        [cmd, filename], capture_output=True, check=True, encoding=encoding
    )
    string = str(result.stdout)
    lines = string.splitlines()

    if lines[1] != "s SATISFIABLE":
        return False, []

    model = lines[2][2:].split(" ")

    return True, [int(x) for x in model]


# Fonctions utilitaires de conversions


def cell_to_variable(i: int, j: int, val: int) -> int:
    return 81 * i + 9 * j + val


def variable_to_cell(var: int) -> Tuple[int, int, int]:
    var -= 1
    i = var // 81
    j = var % 81 // 9
    var = var % 81 % 9 + 1
    return (i, j, var)


# Génération du problème


def at_least_one(vars: List[int]) -> List[int]:
    return vars[:]


def unique(vars: List[int]) -> List[List[int]]:
    r = []
    r.append(at_least_one(vars))
    for l in iter.combinations(vars, 2):
        l = [-x for x in l]
        r.append(l)
    return r


def create_cell_constraints() -> List[List[int]]:
    list = []
    for i in range(9):
        for j in range(9):
            raw = [
                x
                for x in range(cell_to_variable(i, j, 1), cell_to_variable(i, j, 9) + 1)
            ]
            list += unique(raw)
    return list


def create_line_constraints() -> List[List[int]]:
    list = []
    for c in range(1, 10):
        for i in range(9):
            sublist = []
            for j in range(9):
                sublist.append(cell_to_variable(i, j, c))
            list.append(sublist)
    return list


def create_column_constraints() -> List[List[int]]:
    list = []
    for c in range(1, 10):
        for j in range(9):
            sublist = []
            for i in range(9):
                sublist.append(cell_to_variable(i, j, c))
            list.append(sublist)
    return list


def create_box_constraints() -> List[List[int]]:
    list = []
    for bi in range(3):
        for bj in range(3):
            for c in range(1, 10):
                sublist = []
                for i in range(3):
                    for j in range(3):
                        sublist.append(cell_to_variable(i + (bi * 3), j + (bj * 3), c))
                list.append(sublist)
    return list


def create_value_constraints(grid: List[List[int]]) -> List[List[int]]:
    list = []
    for i in range(len(grid)):
        for j in range(len(grid)):
            if grid[i][j] > 0:
                list.append([cell_to_variable(i, j, grid[i][j])])
    return list


def generate_problem(grid: List[List[int]]) -> List[List[int]]:
    list = []
    list += create_cell_constraints()
    list += create_box_constraints()
    list += create_line_constraints()
    list += create_column_constraints()
    list += create_value_constraints(grid)
    return list


# Appel solver


def clauses_to_dimacs(clauses: List[List[int]], nb_vars: int) -> str:
    o = f"p cnf {nb_vars} {len(clauses)}\n"
    for l in clauses:
        for x in l:
            o += f"{x} "
        o += "0\n"
    return o


# Conversion et Affichage


def model_to_grid(model: List[int], nb_vals: int = 9) -> List[List]:
    list = empty_grid[:]
    for x in model:
        if x > 0:
            raw = variable_to_cell(x)
            list[raw[0]][raw[1]] = raw[2]
    return list


def print_sudoku(grille: List[List[int]]) -> None:
    print("-------------------------")
    di = 0
    for i in grille:
        if di == 3:
            print("-------------------------")
            di = 0
        o = "| "
        dj = 0
        for j in i:
            if j == 0:
                j = "."
            if dj == 3:
                o += f"| {j} "
                dj = 1
            else:
                o += f"{j} "
                dj += 1
        o += "| "
        print(o)
        di += 1
    print("-------------------------")


# Nouvelle fonction pour implémentation du jeu

def solve_sudoku(sudoku: List[List[int]]) -> Tuple[bool, List[List[int]]]:
    clauses = generate_problem(sudoku)
    write_dimacs_file(clauses_to_dimacs(clauses, 729), "dimacs.cnf")

    result = exec_gophersat("dimacs.cnf")
    if not result[0]:
        return False, None
    else:
        grilleResult = model_to_grid(result[1])
        return True, grilleResult

# Fonction principale

def main():
    # Print Sudoku Non résolu
    print("Sudoku non résolu:")
    print_sudoku(example2)

    # Résolution
    clauses = generate_problem(example2)
    write_dimacs_file(clauses_to_dimacs(clauses, 729), "dimacs.cnf")

    # Parsing et Affichage
    result = exec_gophersat("dimacs.cnf")
    if not result[0]:
        print("Sudoku non résolvable")
    else:
        grilleResult = model_to_grid(result[1])
        print("Sudoku résolu:")
        print_sudoku(grilleResult)


if __name__ == "__main__":
    main()
